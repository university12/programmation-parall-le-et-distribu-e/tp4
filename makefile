.PHONY: build clear

build :
	mkdir -p build
	cd build && cmake ./.. && make
clear:
	rm -rf ./build
run:
	sudo ./build/tp4
