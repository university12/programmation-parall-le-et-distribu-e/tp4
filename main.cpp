// System includes
#define __CL_ENABLE_EXCEPTIONS
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

using namespace std;

// OpenCL includes
#include <CL/cl2.hpp>

// Matrix
#include "Matrix.hpp"

// Project includes

// Constants, globals
const int ELEMENTS = 2048; // elements in each vector

// Signatures
char *readSource(const char *sourceFilename);
void initializeCMatrix(double **matrix, int size);
void printCMatrix(double **matrix);
void printCMatrix(double **matrix, int n) {
  int i, j;

  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      printf("%f ", matrix[i][j]);
    }
    printf("\n");
  }
}
void initializeCMatrix(double **matrix, int size) {
  for (int i = 0; i < size; i++) {
    matrix[i] = (double *)malloc(size * sizeof(double));
  }
}
int main(int argc, char **argv) {
  srand((unsigned)time(NULL));

  unsigned int lS = 5;
  if (argc == 2) {
    lS = atoi(argv[1]);
  }
  double **c_matrix = (double **)malloc(lS * sizeof(double *));

  initializeCMatrix(c_matrix, lS);

  MatrixRandom lA(lS, lS);
  cout << "Matrice random:\n" << lA.str() << endl;
  lA.toCMatrix(c_matrix, lS);
  printCMatrix(c_matrix, lS);

  printf("Running Invert matrix\n\n");

  // Length of vectors
  unsigned int n = 1000;

  // Host input vectors
  double *h_a;
  double *h_b;
  // Host output vector
  double *h_c;

  // Device input buffers
  cl::Buffer d_a;
  cl::Buffer d_b;
  // Device output buffer
  cl::Buffer d_c;

  // Size, in bytes, of each vector
  size_t bytes = n * sizeof(double);

  // Allocate memory for each vector on host
  h_a = new double[n];
  h_b = new double[n];
  h_c = new double[n];

  // Initialize vectors on host
  for (int i = 0; i < n; i++) {
    h_a[i] = i;
    h_b[i] = i - 2;
  }

  cl_int err = CL_SUCCESS;
  try {

    // Query platforms
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    if (platforms.size() == 0) {
      std::cout << "Platform size 0\n";
      return -1;
    }

    // Get list of devices on default platform and create context
    cl_context_properties properties[] = {
        CL_CONTEXT_PLATFORM, (cl_context_properties)(platforms[0])(), 0};
    cl::Context context(CL_DEVICE_TYPE_GPU, properties);
    std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();

    // Create command queue for first device
    cl::CommandQueue queue(context, devices[0], 0, &err);

    // Create device memory buffers
    d_a = cl::Buffer(context, CL_MEM_READ_ONLY, bytes);
    d_b = cl::Buffer(context, CL_MEM_READ_ONLY, bytes);
    d_c = cl::Buffer(context, CL_MEM_WRITE_ONLY, bytes);

    // Bind memory buffers
    queue.enqueueWriteBuffer(d_a, CL_TRUE, 0, bytes, h_a);
    queue.enqueueWriteBuffer(d_b, CL_TRUE, 0, bytes, h_b);

    // Build kernel from source string
    std::vector<std::string> c_string_program = {readSource("invert.cl")};
    cl::Program::Sources source(c_string_program);
    cl::Program program_ = cl::Program(context, source);
    program_.build(devices);

    // Create kernel object
    cl::Kernel kernel(program_, "vecAdd", &err);

    // Bind kernel arguments to kernel
    kernel.setArg(0, d_a);
    kernel.setArg(1, d_b);
    kernel.setArg(2, d_c);
    kernel.setArg(3, n);

    // Number of work items in each local work group
    cl::NDRange localSize(64);
    // Number of total work items - localSize must be devisor
    cl::NDRange globalSize((int)(ceil(n / (float)64) * 64));

    // Enqueue kernel
    cl::Event event;
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, globalSize, localSize,
                               NULL, &event);

    // Block until kernel completion
    event.wait();

    // Read back d_c
    queue.enqueueReadBuffer(d_c, CL_TRUE, 0, bytes, h_c);
  } catch (cl::Error err) {
    std::cerr << "ERROR: " << err.what() << "(" << err.err() << ")"
              << std::endl;
  }

  // Sum up vector c and print result divided by n, this should equal 1 within
  // error
  double sum = 0;
  for (int i = 0; i < n; i++)
    sum += h_c[i];
  std::cout << "final result: " << sum / n << std::endl;

  // Release host memory
  delete (h_a);
  delete (h_b);
  delete (h_c);
  return 0;
}

char *readSource(const char *sourceFilename) {

  FILE *fp;
  int err;
  int size;

  char *source;

  fp = fopen(sourceFilename, "rb");
  if (fp == NULL) {
    printf("Could not open kernel file: %s\n", sourceFilename);
    exit(-1);
  }

  err = fseek(fp, 0, SEEK_END);
  if (err != 0) {
    printf("Error seeking to end of file\n");
    exit(-1);
  }

  size = ftell(fp);
  if (size < 0) {
    printf("Error getting file position\n");
    exit(-1);
  }

  err = fseek(fp, 0, SEEK_SET);
  if (err != 0) {
    printf("Error seeking to start of file\n");
    exit(-1);
  }

  source = (char *)malloc(size + 1);
  if (source == NULL) {
    printf("Error allocating %d bytes for the program source\n", size + 1);
    exit(-1);
  }

  err = fread(source, 1, size, fp);
  if (err != size) {
    printf("only read %d bytes\n", err);
    exit(0);
  }

  source[size] = '\0';

  return source;
}
